<!doctype html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>Document</title>
</head>
<body>

    @include('flash::message')

    {{ Form::model($_resource, [
        'url'    => $_url,
        'method' => $_method,
        'files'  => $_files,
    ]) }}
    @include($_view)
    {{ Form::close() }}

</body>
</html>
