<?php namespace Sahib\Elegan\Models;

use Sahib\Elegan\Exceptions\UndefinedImageSizeException;
use Sahib\Elegan\Support\ImageHandler;

/**
 * Class ModelWithImages
 * @package Sahib\Elegan\Models
 */
abstract class ModelWithImages extends ModelWithFiles
{
    /**
     * ImageHandler instance.
     *
     * @var \Sahib\Elegan\Support\ImageHandler
     */
    private $imageHandler;

    public static function boot()
    {
        parent::boot();

        static::saving(function ($resource)
        {
            $resource->makeImages();
        });
    }

    /**
     * Get the ImageHandler instance
     *
     * @return \Sahib\Elegan\Support\ImageHandler
     */
    private function getImageHandler()
    {
        if (is_null($this->imageHandler))
        {
            $this->imageHandler = new ImageHandler();
        }

        return $this->imageHandler;
    }

    /**
     * Return the asset URL of an image, optionaly resized.
     *
     * @param string      $attribute
     * @param string|null $size
     * @param bool|null   $secure
     *
     * @return string
     */
    public function image($attribute, $size = null, $secure = null)
    {
        $imagePath = $this->getPath($attribute);

        // Use the default image if the original is missing.
        if (!is_file($imagePath))
        {
            $imagePath = $this->getPathDefault($attribute);
        }

        if (!is_null($size) && $size != 'full')
        {
            // Generate the resized image if not exists.
            $sizeValues = $this->getConfig()->get("$attribute.image_sizes.$size");

            if (is_null($sizeValues))
            {
                throw new UndefinedImageSizeException("Image size \"$size\" is not defined on configuration file.");
            }

            $sizedImagePath = $this->getImageHandler()->appendSize($imagePath, $sizeValues);

            if (!is_file($sizedImagePath))
            {
                $this->getImageHandler()->resize($imagePath, $sizedImagePath, $sizeValues);
            }

            $imagePath = $sizedImagePath;
        }

        // Strip the public path.
        $url = str_replace(public_path(), '', $imagePath);

        return asset($url, $secure);
    }

    /**
     * Delete a file related to this record.
     * If $old is true, the name of the file is taken from
     * the getOriginal() method.
     *
     * @param string $attribute
     * @param bool   $old
     */
    public function deleteFile($attribute, $old = false)
    {
        // Delete the original file.
        parent::deleteFile($attribute, $old);

        // Delete the resized images associated with the attribute.
        $this->deleteResizedImages($attribute, $old);
    }

    /**
     * Delete all the resized images associated with an attribute.
     *
     * @param string $attribute
     * @param bool   $old
     */
    private function deleteResizedImages($attribute, $old)
    {
        // Retrieve the configuration for the current attribute.
        $config = $this->getConfig()->get($attribute);

        // If this attribute is defined "as image" then process to delete the files.
        if (isset($config['is_image']) && $config['is_image'])
        {
            // Get all image sizes, except full.
            $sizes = $this->getConfig()->get("$attribute.image_sizes");
            $sizes = array_except($sizes, ['full']);

            // Path where the image should be stored.
            $path = $this->getConfig()->get("$attribute.path");

            // The image name, if $old then return the original value of the attribute.
            $imageName = $old ? $this->getOriginal($attribute) : $this->$attribute;

            foreach ($sizes as $size)
            {
                $sizedImageName = $this->getImageHandler()->appendSize($imageName, $size);

                delete_file("$path/$sizedImageName");
            }
        }
    }

    public function makeImages()
    {
        $changedFiles = $this->getChangedFiles();

        foreach ($changedFiles as $attribute => $file)
        {
            if ($this->getConfig()->isImage($attribute))
            {
                $imagePath = $this->getPath($attribute);
                $size = $this->getConfig()->get("$attribute.image_sizes.full");

                // Generate full sized image.
                $this->getImageHandler()->resize($imagePath, $imagePath, $size);
            }
        }

    }
}
