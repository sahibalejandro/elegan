<?php namespace Sahib\Elegan\Models;

use Eloquent;
use Sahib\Elegan\Support\ModelConfig;

/**
 * Class ModelWithFiles
 * @package Sahib\Elegan
 */
abstract class ModelWithFiles extends Eloquent
{
    /**
     * ModelConfig instance.
     *
     * @var \Sahib\Elegan\Support\ModelConfig
     */
    private $modelConfig;

    /**
     * Boot model.
     */
    public static function boot()
    {
        parent::boot();

        /**
         * "Saving" event.
         */
        static::saving(function (Eloquent $resource)
        {
            // When a model is going to be updated, we look for changed (dirty)
            // attributes, and if that attributes are "file attributes" then
            // we delete the old file.
            if ($resource->exists)
            {
                $changedFiles = $resource->getChangedFiles();

                foreach ($changedFiles as $attribute => $file)
                {
                    $resource->deleteFile($attribute, true);
                }
            }
        });

        /**
         * "Deleted" event.
         */
        static::deleted(function (Eloquent $resource)
        {
            // Delete all files related to this record.
            $resource->deleteAllFiles();
        });
    }

    /**
     * Return the absolute path of the associated file.
     *
     * @param string $attribute
     *
     * @return string
     */
    public function getPath($attribute)
    {
        $path = $this->getConfig()->get("$attribute.path");

        return "$path/{$this->$attribute}";
    }

    /**
     * Return the path for the default file defined in the configuration.
     *
     * @param string $attribute
     *
     * @return string
     */
    public function getPathDefault($attribute)
    {
        $path = $this->getConfig()->get("$attribute.path");
        $fileName = $this->getConfig()->get("$attribute.default_file");

        return "$path/$fileName";
    }

    /**
     * Check if the associated file exists.
     *
     * @param string $attribute
     *
     * @return bool
     */
    public function fileExists($attribute)
    {
        $path = $this->getPath($attribute);

        return is_file($path);
    }

    /**
     * Return the asset URL.
     *
     * @param string    $attribute
     * @param bool|null $secure
     *
     * @return string
     */
    public function asset($attribute, $secure = null)
    {
        $path = $this->getPath($attribute);

        $url = str_replace(public_path(), '', $path);

        return asset($url, $secure);
    }

    /**
     * Delete a file related to this record.
     * If $old is true, the name of the file is taken from
     * the getOriginal() method.
     *
     * @param string $attribute
     * @param bool   $old
     */
    public function deleteFile($attribute, $old = false)
    {
        $path = $this->getConfig()->get("$attribute.path");

        $file = $old ? $this->getOriginal($attribute) : $this->$attribute;

        delete_file("$path/$file");
    }

    /**
     * Delete all files related to this record.
     */
    public function deleteAllFiles()
    {
        $attributes = $this->getConfig()->attributes();

        foreach ($attributes as $attribute)
        {
            $this->deleteFile($attribute);
        }
    }

    /**
     * Return a key/value array with the list of changed
     * files on the model.
     *
     * @return array
     */
    public function getChangedFiles()
    {
        $changed = [];

        $dirty = $this->getDirty();
        $attributes = $this->getConfig()->attributes();

        foreach ($attributes as $attribute)
        {
            if (array_key_exists($attribute, $dirty))
            {
                $changed[$attribute] = $dirty[$attribute];
            }
        }

        return $changed;
    }

    /**
     * Get the ModelConfig instance related to this model.
     *
     * @return \Sahib\Elegan\Support\ModelConfig
     */
    public function getConfig()
    {
        if (is_null($this->modelConfig))
        {
            $this->modelConfig = new ModelConfig($this);
        }

        return $this->modelConfig;
    }

}
