<?php namespace Sahib\Elegan\Controllers;

use View;
use Flash;
use Input;
use Eloquent;
use Redirect;
use Request;
use Illuminate\Support\MessageBag;
use Sahib\Elegan\Support\Uploader;
use Sahib\Elegan\Validation\InputValidatorException;

/**
 * Class ResourceController
 * @package Sahib\Elegan
 */
class ResourceController extends \BaseController
{
    /**
     * An instance of Repository
     *
     * @var \Sahib\Elegan\Repositories\Repository
     */
    protected $repository;

    /**
     * An instance of InputValidator.
     *
     * @var \Sahib\Elegan\Validation\InputValidator
     */
    protected $validator;

    /**
     * Name of the variable that will contain a resource
     * in the views.
     *
     * @var string
     */
    protected $resource = 'resource';

    /**
     * Name of the variable that will contain a collection
     * of resources in the views.
     *
     * @var string
     */
    protected $resources = 'resources';

    /**
     * Views prefix.
     *
     * @var string
     */
    protected $viewsPrefix = 'resource';

    /**
     * Routes prefix.
     *
     * @var string
     */
    protected $routesPrefix = 'resource';

    /**
     * Whether to use file uploads or not.
     *
     * @var bool
     */
    protected $files = false;

    /**
     * Show a paginated list of resources.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $resources = $this->repository->paginate();

        return $this->view('index')->with([
            "{$this->resources}" => $resources,
        ]);
    }

    /**
     * Show form for create a resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return $this->editForm();
    }

    /**
     * Store a new resource in database.
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        try
        {
            $this->validator->createMode()->validate(Input::all());
        } catch (InputValidatorException $e)
        {
            return $this->failedValidationResponse($e->getErrors());
        }

        if ($this->files)
        {
            $uploads = $this->moveUploadedFiles();
            Input::merge($uploads);
        }

        $resource = $this->repository->create(Input::all());

        return $this->createdResourceResponse($resource);
    }

    /**
     * Show the view to edit a resource.
     *
     * @param int $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $resource = $this->repository->find($id);

        return $this->editForm($resource);
    }

    /**
     * Update a resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        // Get the resource to update
        $resource = $this->repository->find($id);

        try
        {
            $this->validator->updateMode($resource)->validate(Input::all());
        } catch (InputValidatorException $e)
        {
            return $this->failedValidationResponse($e->getErrors());
        }

        if ($this->files)
        {
            // Delete requested files and update the resource.
            $this->deleteRequestedFiles($resource);

            // Move uploaded files and update the input.
            $withUploadedFiles = $this->moveUploadedFiles();
            Input::merge($withUploadedFiles);
        }

        // Update the resource.
        $this->repository->update($resource, Input::all());

        return $this->updatedResourceResponse($resource);
    }

    /**
     * Destroy a resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $resource = $this->repository->destroy($id);

        return $this->destroyedResourceResponse($resource);
    }

    /**
     * Moves uploaded files to their respective directories and
     * generates an array of key/value pairs where the key is
     * the name of the model's attribute related with the uploaded
     * file, and the value is the name of the uploaded file.
     *
     * @return array
     */
    private function moveUploadedFiles()
    {
        $uploader = new Uploader($this->repository->getModel(), Request::instance());

        return $uploader->moveUploads();
    }

    /**
     * Return a view, the $name is prefixed with the view prefix.
     *
     * @param string $name
     *
     * @return \Illuminate\View\View
     */
    protected function view($name)
    {
        return View::make($this->viewsPrefix . ".$name");
    }

    /**
     * Return a prefixed route name.
     *
     * @param string $route
     *
     * @return string
     */
    protected function routeName($route)
    {
        return $this->routesPrefix . ".$route";
    }

    /**
     * Return the response to show the edit form for a resource.
     *
     * @param \Eloquent $resource
     *
     * @return \Illuminate\View\View
     */
    private function editForm(Eloquent $resource = null)
    {
        if (is_null($resource))
        {
            $url = route($this->routeName('index'));
        }
        else
        {
            $url = route($this->routeName('update'), $resource->id);
        }

        return View::make('elegan::partials.resource_form')->with([
            '_url'              => $url,
            '_method'           => $resource ? 'put' : 'post',
            '_files'            => $this->files,
            '_view'             => $this->viewsPrefix . '.form',

            '_resource'         => $resource,
            "{$this->resource}" => $resource,
        ]);
    }

    /**
     * Return the response when an input validation fails.
     *
     * @param \Illuminate\Support\MessageBag $errors
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function failedValidationResponse(MessageBag $errors)
    {
        $messages = implode($errors->all('<li>:message</li>'));
        Flash::error("<ul>$messages</ul>");

        return Redirect::back()->withInput()->withErrors($errors);
    }

    /**
     * Return the response when a resource is created.
     *
     * @param \Eloquent $resource
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function createdResourceResponse(Eloquent $resource)
    {
        $resource = ucwords(str_replace('_', ' ', $this->resource));

        Flash::message("New $resource created!");

        return Redirect::route($this->routeName('index'));
    }

    /**
     * Return the response when a resource is updated.
     *
     * @param \Eloquent $resource
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function updatedResourceResponse(Eloquent $resource)
    {
        $resource = ucwords(str_replace('_', ' ', $this->resource));

        Flash::message("$resource updated!");

        return Redirect::route($this->routeName('index'));
    }

    /**
     * Return the response when a resource is deleted.
     *
     * @param \Eloquent $resource
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function destroyedResourceResponse(Eloquent $resource)
    {
        $resource = ucwords(str_replace('_', ' ', $this->resource));

        Flash::message("$resource deleted!");

        return Redirect::route($this->routeName('index'));
    }

    /**
     * Delete the files specified in the request input as "delete_<attribute>_file"
     * and update the resource with the empty values on the attributes which files
     * was deleted.
     *
     * @param \Eloquent $resource
     */
    private function deleteRequestedFiles(Eloquent $resource)
    {
        $inputKeys = array_keys(Input::all());

        foreach ($inputKeys as $inputKey)
        {
            if (preg_match('/^delete_(\w+)_file$/', $inputKey, $matches))
            {
                $attribute = $matches[1];

                $resource->deleteFile($attribute);
                $resource->$attribute = '';
            }
        }

        $resource->save();
    }
}
