<?php namespace Sahib\Elegan\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

class MakeCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'elegan:make';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create Elegan Resource.';

    protected $buildersOptions = [
        'controller' => 'Sahib\Elegan\Commands\Builders\ControllerScriptBuilder',
        'model'      => 'Sahib\Elegan\Commands\Builders\ModelScriptBuilder',
        'repository' => 'Sahib\Elegan\Commands\Builders\RepositoryScriptBuilder',
        'validator'  => 'Sahib\Elegan\Commands\Builders\InputValidatorScriptBuilder',
    ];

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $name = $this->argument('name');
        $namespace = $this->argument('namespace');
        $path = $this->option('path');

        $builders = $this->getScriptBuilders($name, $namespace, $path);

        try
        {
            $this->checkScriptsExistance($builders);

            $this->buildScripts($builders);

            $this->info('Elegan files was created!');
        } catch (FileException $e)
        {
            $this->error($e->getMessage());
        }

    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['name', InputArgument::REQUIRED, 'Name of the resource'],
            ['namespace', InputArgument::OPTIONAL, 'Namespace', 'App'],
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['path', null, InputOption::VALUE_OPTIONAL, 'Base path where files should be stored.', 'app'],

            ['controller', null, InputOption::VALUE_NONE, 'Make controller'],
            ['model', null, InputOption::VALUE_NONE, 'Make model'],
            ['repository', null, InputOption::VALUE_NONE, 'Make repository'],
            ['validator', null, InputOption::VALUE_NONE, 'Make validator']
        ];
    }

    /**
     * @param $builders
     *
     * @return mixed
     */
    protected function checkScriptsExistance($builders)
    {
        foreach ($builders as $builder)
        {
            if (file_exists($builder->filePath()))
            {
                throw new FileException("File " . $builder->filePath() . " already exists and I don't want to overwrite it.");
            }
        }
    }

    /**
     * @param $builders
     */
    protected function buildScripts($builders)
    {
        foreach ($builders as $builder)
        {
            $this->info('Writing ' . $builder->filePath() . ' ...');
            $builder->build();
        }
    }

    /**
     * Return an array of script builders based on the passed options to the command.
     *
     * @param $name
     * @param $namespace
     * @param $path
     *
     * @return array
     */
    protected function getScriptBuilders($name, $namespace, $path)
    {
        $builders = [];

        $allAssets = $this->checkForNoAssetsOption();

        foreach ($this->buildersOptions as $asset => $className)
        {
            if ($allAssets || $this->option($asset))
            {
                $builders[] = new $className($name, $namespace, $path);
            }
        }

        return $builders;
    }

    /**
     * Return true if none asset option is defined.
     *
     * @return bool
     */
    protected function checkForNoAssetsOption()
    {
        $builderKeys = array_keys($this->buildersOptions);

        foreach ($builderKeys as $key)
        {
            if ($this->option($key)) return false;
        }

        return true;
    }

}
