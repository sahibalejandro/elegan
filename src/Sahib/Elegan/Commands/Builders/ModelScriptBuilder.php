<?php namespace Sahib\Elegan\Commands\Builders;

class ModelScriptBuilder extends ScriptBuilder
{

    public function className()
    {
        return $this->name;
    }

    public function dir()
    {
        return 'Models';
    }

    public function templateData()
    {
        return [
            'class'     => $this->className(),
            'namespace' => $this->getNamespace(),
        ];
    }

    public function template()
    {
        return 'Model.txt';
    }

    public function getNamespace()
    {
        return $this->namespace . '\Models';
    }
}
