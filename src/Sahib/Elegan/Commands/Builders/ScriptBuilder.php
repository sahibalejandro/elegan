<?php namespace Sahib\Elegan\Commands\Builders;

use Mustache_Engine;

/**
 * Class ScriptBuilder
 * @package Sahib\Elegan\Commands\Builders
 */
abstract class ScriptBuilder
{

    /**
     * Resource name.
     *
     * @var string
     */
    protected $name;
    /**
     * Path where to store the file.
     *
     * @var string
     */
    protected $path;
    /**
     * @var string
     */
    protected $namespace;
    /**
     * @var string
     */
    protected $namespacePath;
    /**
     * @var \Mustache_Engine
     */
    private $mustache;

    /**
     * @return mixed
     */
    abstract public function templateData();

    /**
     * @return mixed
     */
    abstract public function template();

    /**
     * @return mixed
     */
    abstract public function className();

    /**
     * @return mixed
     */
    abstract public function getNamespace();

    /**
     * @return mixed
     */
    abstract public function dir();

    /**
     * @param $name
     * @param $namespace
     * @param $basePath
     */
    public function __construct($name, $namespace, $basePath)
    {
        // Sanitize input data.
        $basePath = rtrim($basePath, '/');
        $namespace = trim($namespace, '\\');
        $namespacePath = $basePath . '/' . str_replace('\\', '/', $namespace);

        $this->name = $name;
        $this->namespace = $namespace;

        $this->path = $basePath;
        $this->namespacePath = $namespacePath;

        $this->mustache = new Mustache_Engine;
    }

    /**
     * @return bool
     */
    public function exists()
    {
        return file_exists($this->filePath());
    }

    /**
     * @return string
     */
    public function filePath()
    {
        $dir = $this->dir();
        $class = $this->className();

        return "{$this->namespacePath}/{$dir}/{$class}.php";
    }

    /**
     *
     */
    public function build()
    {
        $template = file_get_contents(__DIR__.'/../../Templates/' . $this->template());

        $render = $this->mustache->render($template, $this->templateData());

        // Create directory if not exists.
        $dir = dirname($this->filePath());
        if (!file_exists($dir)) mkdir($dir, 0777, true);

        file_put_contents($this->filePath(), $render);
    }

}
