<?php namespace Sahib\Elegan\Commands\Builders;

class ControllerScriptBuilder extends ScriptBuilder
{

    public function className()
    {
        return str_plural($this->name) . 'Controller';
    }

    public function dir()
    {
        return 'Controllers';
    }

    public function templateData()
    {
        $resource = snake_case($this->name);

        $repositoryBuilder = new RepositoryScriptBuilder($this->name, $this->namespace, $this->path);
        $validatorBuilder = new InputValidatorScriptBuilder($this->name, $this->namespace, $this->path);

        return [
            'class'                => $this->className(),
            'namespace'            => $this->getNamespace(),
            'resource'             => $resource,
            'resources'            => str_plural($resource),
            'repository_namespace' => $repositoryBuilder->getNamespace(),
            'repository_class'     => $repositoryBuilder->className(),
            'validator_namespace'  => $validatorBuilder->getNamespace(),
            'validator_class'      => $validatorBuilder->className(),
        ];
    }

    public function template()
    {
        return 'Controller.txt';
    }

    public function getNamespace()
    {
        return $this->namespace . '\Controllers';
    }
}
