<?php namespace Sahib\Elegan\Commands\Builders;

class InputValidatorScriptBuilder extends ScriptBuilder
{

    public function className()
    {
        return $this->name . 'InputValidator';
    }

    public function dir()
    {
        return 'Validation';
    }

    public function templateData()
    {
        return [
            'class'     => $this->className(),
            'namespace' => $this->getNamespace(),
        ];
    }

    public function template()
    {
        return 'InputValidator.txt';
    }

    public function getNamespace()
    {
        return $this->namespace . '\Validation';
    }
}
