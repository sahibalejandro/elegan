<?php namespace Sahib\Elegan\Commands\Builders;

/**
 * Class RepositoryScriptBuilder
 * @package Sahib\Elegan\Commands\Builders
 */
class RepositoryScriptBuilder extends ScriptBuilder
{

    /**
     * @return string
     */
    public function className()
    {
        return str_plural($this->name) . 'Repository';
    }

    /**
     * @return string
     */
    public function dir()
    {
        return 'Repositories';
    }

    /**
     * @return array
     */
    public function templateData()
    {
        $model = $this->getModelFullName();

        return [
            'class'     => $this->className(),
            'namespace' => $this->getNamespace(),
            'model'     => $model,
        ];
    }

    /**
     * @return string
     */
    public function template()
    {
        return 'Repository.txt';
    }

    /**
     * @return string
     */
    public function getNamespace()
    {
        return $this->namespace . '\Repositories';
    }

    /**
     * Get fully qualified model name.
     *
     * @return string
     */
    private function getModelFullName()
    {
        $model = $this->name;

        $namespace = explode("\\", $this->getNamespace());
        array_pop($namespace);

        $namespace[] = 'Models';
        $namespace[] = $model;

        return implode("\\", $namespace);
    }
}
