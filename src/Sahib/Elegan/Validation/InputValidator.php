<?php namespace Sahib\Elegan\Validation;

/**
 * Class InputValidator
 * @package Sahib\Elegan\Validation
 */
class InputValidator
{
    /**
     * Validation rules.
     *
     * @var array
     */
    protected $rules = [];

    /**
     * Validation messgaes.
     *
     * @var array
     */
    protected $messages = [];

    /**
     * @var \Illuminate\Validation\Validator
     */
    protected $validation;

    /**
     * Append a validation rule.
     *
     * @param string $input
     * @param string $rule
     */
    public function appendRule($input, $rule)
    {
        if ($this->rulesAreString($input))
        {
            $this->rules[$input] .= "|{$rule}";
        }
        else
        {
            $this->rules[$input][] = $rule;
        }
    }

    /**
     * Removes a validation rule from a group of already defined validation rules.
     *
     * @param string $input
     * @param string $rule
     */
    public function removeRule($input, $rule)
    {
        if ($this->rulesAreString($input))
        {
            $this->removeRuleFromString($input, $rule);
        }
        else
        {
            $this->removeRuleFromArray($input, $rule);
        }
    }

    /**
     * Removes a validation rule from a string of validation rules.
     *
     * @param string $input
     * @param string $rule
     */
    private function removeRuleFromString($input, $rule)
    {
        $rules = explode('|', $this->rules[$input]);

        $idx = array_search($rule, $rules);

        if ($idx !== false) unset($rules[$idx]);

        $this->rules[$input] = implode('|', $rules);
    }

    /**
     * Removes a validation rule from an array of validation rules.
     *
     * @param string $input
     * @param string $rule
     */
    private function removeRuleFromArray($input, $rule)
    {
        $idx = array_search($rule, $this->rules[$input]);

        if ($idx !== false) unset($this->rules[$input][$idx]);

        // Reset array indexes to pass unit tests.
        $this->rules[$input] = array_values($this->rules[$input]);
    }

    /**
     * Set a new validation or replace an already defined one.
     *
     * @param string $input
     * @param string $rules
     */
    public function set($input, $rules)
    {
        $this->rules[$input] = $rules;
    }

    /**
     * Remove a validation.
     *
     * @param string $input
     */
    public function remove($input)
    {
        unset($this->rules[$input]);
    }

    /**
     * Setup validator for create mode.
     *
     * @return $this
     */
    public function createMode()
    {
        return $this;
    }

    /**
     * Setup validator for update mode.
     *
     * @param \Eloquent $resource
     *
     * @return $this
     */
    public function updateMode(\Eloquent $resource)
    {
        return $this;
    }

    /**
     * Validate input data.
     *
     * @param array $input
     *
     * @throws \Sahib\Elegan\Validation\InputValidatorException
     */
    public function validate(array $input)
    {
        $this->validation = \Validator::make($input, $this->rules, $this->messages);

        if ($this->validation->fails())
        {
            throw new InputValidatorException($this->validation->errors(), 'Validation Fails!');
        }
    }

    /**
     * Return the validation rules.
     *
     * @return array
     */
    public function getRules()
    {
        return $this->rules;
    }

    /**
     * @param $input
     *
     * @return bool
     */
    protected function rulesAreString($input)
    {
        return is_string($this->rules[$input]);
    }

}
