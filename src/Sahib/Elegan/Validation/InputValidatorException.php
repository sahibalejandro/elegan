<?php namespace Sahib\Elegan\Validation;

use Exception;
use Illuminate\Support\MessageBag;

/**
 * Class InputValidatorException
 * @package Sahib\Elegan\Validation
 */
class InputValidatorException extends \Exception {

    /**
     * @var \Illuminate\Support\MessageBag
     */
    private $errors;

    /**
     * @param \Illuminate\Support\MessageBag $errors
     * @param string                         $message
     * @param int                            $code
     * @param \Exception                     $previous
     */
    public function __construct(MessageBag $errors, $message = "", $code = 0, Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
        $this->errors = $errors;
    }

    /**
     * @return MessageBag
     */
    public function getErrors()
    {
        return $this->errors;
    }

}
