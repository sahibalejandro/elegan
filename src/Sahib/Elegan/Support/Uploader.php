<?php namespace Sahib\Elegan\Support;

use Str;
use Illuminate\Http\Request;

class Uploader
{

    /**
     * Instance of ModelConfig.
     *
     * @var \Sahib\Elegan\Support\ModelConfig
     */
    private $config;

    /**
     * The request where to get the uploaded files.
     *
     * @var \Illuminate\Http\Request
     */
    private $request;

    /**
     * @param string                   $modelClass
     * @param \Illuminate\Http\Request $request
     */
    public function __construct($modelClass, Request $request)
    {
        $this->request = $request;
        $this->config = new ModelConfig($modelClass);
    }

    /**
     * Move all uploaded files to the directories specified in
     * the model's configuration and return an key/value array
     * where the key is the attribute and the value is the name
     * of the uploaded file.
     *
     * @return array
     */
    public function moveUploads()
    {
        $uploaded = [];

        // Get all attributes on the model's configuration.
        $attributes = $this->config->attributes();

        foreach ($attributes as $attribute)
        {
            // The name of the file input field.
            $inputKey = "{$attribute}_file";

            if ($this->request->hasFile($inputKey))
            {
                // Get the UploadedFile instance.
                $inputFile = $this->request->file($inputKey);

                /**
                 * Search for the available file name to store.
                 * The file name will be sanitized.
                 */
                $path = $this->config->get("$attribute.path");
                $fileName = $inputFile->getClientOriginalName();
                $fileName = $this->sanitizeFileName($fileName);
                $fileName = available_file_name($path, $fileName);

                // And just move
                $inputFile->move($path, $fileName);

                // Add to the uploaded array.
                $uploaded[$attribute] = $fileName;
            }
        }

        return $uploaded;
    }

    /**
     * Sanitize a filename.
     *
     * @param string $fileName
     * @return string
     */
    private function sanitizeFileName($fileName)
    {
        $nameParts = explode('.', $fileName);
        $extension = array_pop($nameParts);
        $name      = implode('.', $nameParts);

        $name = Str::slug($name);

        return "{$name}.{$extension}";
    }
}
