<?php namespace Sahib\Elegan\Support;

use Image;

/**
 * Class ImageHandler
 * @package Sahib\Elegan\Support
 */
class ImageHandler
{
    /**
     * Resize an image.
     *
     * @param string $srcImageFile
     * @param string $dstImageFile
     * @param array  $size
     */
    public function resize($srcImageFile, $dstImageFile, array $size)
    {
        if (is_file($srcImageFile))
        {
            $image = Image::make($srcImageFile);
            $image->fit($size[0], $size[1]);
            $image->save($dstImageFile);
        }
    }

    /**
     * Append a size suffix to a file name.
     *
     * @param string $fileName
     * @param array  $size
     *
     * @return string
     */
    public function appendSize($fileName, array $size)
    {
        $suffix = "-{$size[0]}x{$size[1]}";

        return preg_replace('/(\.\w+)$/', "$suffix$1", $fileName);
    }
}
