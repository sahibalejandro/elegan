<?php namespace Sahib\Elegan\Support;

/**
 * Class ModelConfig
 * @package Sahib\Elegan\Support
 */
class ModelConfig
{
    /**
     * Loaded model's configuration.
     *
     * @var array
     */
    private $config;

    /**
     * Retrieve an instance.
     *
     * @param $model
     *
     * @return static
     */
    public static function of($model)
    {
        return new static($model);
    }

    /**
     * Load model configuration.
     *
     * @param \Sahib\Elegan\Models\ModelWithFiles|string $class
     */
    public function __construct($class)
    {
        $classBasename = class_basename($class);

        $this->config = \Config::get("elegan::$classBasename");
    }

    /**
     * Get a configuration value.
     *
     * @param string|null $key
     *
     * @return mixed
     */
    public function get($key = null)
    {
        return array_get($this->config, $key);
    }

    /**
     * Return an array with the names of the attributes
     * in the configuration file.
     *
     * @return array
     */
    public function attributes()
    {
        return array_keys($this->config);
    }

    /**
     * Checks if an attribute is defined as image.
     *
     * @param string $attribute
     *
     * @return bool
     */
    public function isImage($attribute)
    {
        $config = $this->get($attribute);

        return (isset($config['is_image']) && $config['is_image']);
    }

    /**
     * Shortcut to get the specified image size of an attribute.
     *
     * @param string $attribute
     * @param string $size
     *
     * @return array
     */
    public function imageSize($attribute, $size = 'full')
    {
        return $this->get("$attribute.image_sizes.$size");
    }
}
