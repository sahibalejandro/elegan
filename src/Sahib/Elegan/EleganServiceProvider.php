<?php namespace Sahib\Elegan;

use Illuminate\Support\ServiceProvider;
use Sahib\Elegan\Commands\MakeCommand;

class EleganServiceProvider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Bootstrap the application events.
	 *
	 * @return void
	 */
	public function boot()
	{
		$this->package('sahib/elegan');
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
        $this->registerCommands();
    }

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array();
	}

    /**
     * Register artisan commands.
     */
    protected function registerCommands()
    {
        // Register the elegan:make command.
        $this->app['elegan.make'] = $this->app->share(function ($app)
        {
            return new MakeCommand;
        });

        $this->commands(['elegan.make']);
    }
}
