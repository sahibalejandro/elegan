<?php namespace Sahib\Elegan\Repositories;

use Illuminate\Database\Eloquent\Builder;

/**
 * Class Repository
 * @package Sahib\Elegan\Repositories
 */
abstract class Repository implements RepositoryInterface
{

    /**
     * Model name.
     *
     * @var string
     */
    protected $model = 'Model';

    /**
     * A list of functions to prepare the next query.
     *
     * @see Repository::preapre()
     * @var array(callable)
     */
    private $setUpFunctions = [];

    /**
     * Return all resources.
     *
     * @param array $columns
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function all(array $columns = array('*'))
    {
        return $this->query()->get($columns);
    }

    /**
     * Retrieve a resources paginator.
     *
     * @param int   $perPage
     * @param array $columns
     *
     * @return \Illuminate\Pagination\Paginator
     */
    public function paginate($perPage = 10, array $columns = array('*'))
    {
        return $this->query()->paginate($perPage, $columns);
    }

    /**
     * Find a resource.
     *
     * @param int   $id
     * @param array $columns
     *
     * @return \Illuminate\Database\Eloquent\Model|static
     */
    public function find($id, array $columns = array('*'))
    {
        return $this->query()->find($id, $columns);
    }

    /**
     * Create a resource.
     *
     * @param array $attributes
     *
     * @return \Illuminate\Database\Eloquent\Model|static
     */
    public function create(array $attributes)
    {
        $model = $this->model;

        return $model::create($attributes);
    }

    /**
     * Update a resource.
     *
     * @param int   $resource
     * @param array $attributes
     *
     * @return \Illuminate\Database\Eloquent\Model|static
     */
    public function update($resource, array $attributes)
    {
        if (is_int($resource)) $resource = $this->find($resource);

        $resource->update($attributes);

        return $resource;
    }

    /**
     * Destroy a resource.
     *
     * @param int $id
     *
     * @return mixed
     */
    public function destroy($id)
    {
        $resource = $this->find($id);

        $resource->delete();

        return $resource;
    }

    /**
     * Return a query builder for the model.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function query()
    {
        $model = $this->model;
        $query = $model::query();

        // Customize the query.
        foreach ($this->setUpFunctions as $function)
        {
            call_user_func($function, $query);
        }

        // Clear the setup functions.
        $this->setUpFunctions = [];

        return $query;
    }

    /**
     * Get the name of the associated model.
     *
     * @return string
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * Set a function to set up the next query.
     *
     * @param callable $function
     *
     * @return $this
     */
    public function setUp(callable $function)
    {
        $this->setUpFunctions[] = $function;

        return $this;
    }

    /**
     * Retrieve the first record in the table.
     *
     * @param array $columns
     *
     * @return \Illuminate\Database\Eloquent\Model|static
     */
    public function first(array $columns = ['*'])
    {
        return $this->query()->first($columns);
    }

    /**
     * @param string $method
     * @param array $arguments
     *
     * @return \Sahib\Elegan\Repositories\Repository
     */
    public function __call($method, $arguments)
    {
        // Call with(...) method on the query.
        if (starts_with($method, 'with'))
        {
            $relation = lcfirst(preg_replace('/^with/', '', $method));

            return $this->setUp(function (Builder $query) use ($relation)
            {
                $query->with($relation);
            });
        }

        $class = get_class($this);
        throw new \BadMethodCallException("Method $method does not exists on $class.");
    }
}
