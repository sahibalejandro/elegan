<?php namespace Sahib\Elegan\Repositories;

/**
 * Interface RepositoryInterface
 * @package Sahib\Elegan\Repositories
 */
interface RepositoryInterface
{
    /**
     * Return all resources.
     *
     * @param array $columns
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function all(array $columns);

    /**
     * Retrieve a resources paginator.
     *
     * @param int   $perPage
     * @param array $columns
     *
     * @return \Illuminate\Pagination\Paginator
     */
    public function paginate($perPage, array $columns);

    /**
     * Find a resource.
     *
     * @param int   $id
     * @param array $columns
     *
     * @return \Illuminate\Database\Eloquent\Model|static
     */
    public function find($id, array $columns);

    /**
     * Retrieve the first record in the table.
     *
     * @param array $columns
     *
     * @return \Illuminate\Database\Eloquent\Model|static
     */
    public function first(array $columns);

    /**
     * Create a resource.
     *
     * @param array $attributes
     *
     * @return \Illuminate\Database\Eloquent\Model|static
     */
    public function create(array $attributes);

    /**
     * Update a resource.
     *
     * @param int   $id
     * @param array $attributes
     *
     * @return \Illuminate\Database\Eloquent\Model|static
     */
    public function update($id, array $attributes);

    /**
     * Destroy a resource.
     *
     * @param int $id
     *
     * @return mixed
     */
    public function destroy($id);

    /**
     * Get the name of the associated model.
     *
     * @return string
     */
    public function getModel();
}
