<?php

if (!function_exists('available_file_name'))
{
    /**
     * Search for an available file name in a path.
     *
     * @param string $path
     * @param string $name
     *
     * @return string
     */
    function available_file_name($path, $name)
    {
        $i = 0;
        $newName = $name;

        while (is_file("$path/$newName"))
        {
            $i++;
            $newName = preg_replace('/(\.\w+)$/', "[$i]$1", $name);
        }

        return $newName;
    }
}

if (!function_exists('delete_file'))
{
    /**
     * Delete a file if exists.
     *
     * @param string $file
     */
    function delete_file($file)
    {
        if (is_file($file)) @unlink($file);
    }
}
