# Installation #

    composer require sahib/elegan:~1.5

Add the following service providers to your `config/app.php` file:

    'Sahib\Elegan\EleganServiceProvider',
    'Laracasts\Flash\FlashServiceProvider',
    'Intervention\Image\ImageServiceProvider',

Add the following aliases to your `config/app.php` file:

    'Flash' => 'Laracasts\Flash\Flash',
    'Image' => 'Intervention\Image\Facades\Image',

Publish configuration:

    php artisan config:publish sahib/elegan

Publish views:

    php artisan view:publish sahib/elegan

Create elegan files:

    php artisan elegan:make <Resource> <namespace> [--path=""]
