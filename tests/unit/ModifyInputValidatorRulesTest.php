<?php

use Sahib\Elegan\Validation\InputValidator;

class MyValidator extends InputValidator
{
    protected $rules = [
        'field' => 'foo|bar|baz',
    ];
}

class MyValidatorWithArrayRules extends InputValidator
{
    protected $rules = [
        'field' => ['foo', 'bar', 'baz'],
    ];
}

class ModifyInputValidatorRulesTest extends \Codeception\TestCase\Test
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    // tests
    public function testModifyInputValidatorRules()
    {
        $validator = new MyValidator();

        $this->assertEquals(['field' => 'foo|bar|baz'], $validator->getRules());

        $validator->appendRule('field', 'new');
        $this->assertEquals(['field' => 'foo|bar|baz|new'], $validator->getRules());

        $validator->removeRule('field', 'bar');
        $this->assertEquals(['field' => 'foo|baz|new'], $validator->getRules());

        $validator->set('new_field', 'one|two');
        $this->assertEquals(['field' => 'foo|baz|new', 'new_field' => 'one|two'], $validator->getRules());

        $validator->remove('field');
        $this->assertEquals(['new_field' => 'one|two'], $validator->getRules());
    }

    public function testModifyInputValidatorRulesArray()
    {
        $validator = new MyValidatorWithArrayRules();

        $this->assertEquals(['field' => ['foo', 'bar', 'baz']], $validator->getRules());

        $validator->appendRule('field', 'new');
        $this->assertEquals(['field' => ['foo','bar','baz','new']], $validator->getRules());

        $validator->removeRule('field', 'bar');
        $this->assertEquals(['field' => ['foo','baz','new']], $validator->getRules());
    }

}
