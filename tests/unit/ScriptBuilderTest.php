<?php

use Sahib\Elegan\Commands\Builders\ControllerScriptBuilder;
use Sahib\Elegan\Commands\Builders\InputValidatorScriptBuilder;
use Sahib\Elegan\Commands\Builders\ModelScriptBuilder;
use Sahib\Elegan\Commands\Builders\RepositoryScriptBuilder;

class ScriptBuilderTest extends \Codeception\TestCase\Test
{
   /**
    * @var \UnitTester
    */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    // tests
    public function testControllerScriptBuilderWorks()
    {
        $builder = new ControllerScriptBuilder('Battery', 'Acme', 'app');

        $this->tester->assertEquals('BatteriesController', $builder->className());
        $this->tester->assertEquals('app/Acme/Controllers/BatteriesController.php', $builder->filePath());
        $this->tester->assertEquals('Acme\Controllers', $builder->getNamespace());
    }

    public function testRepositoryScriptBuilderWorks()
    {
        $builder = new RepositoryScriptBuilder('Syllabus', 'Foo', 'app');

        $this->tester->assertEquals('SyllabiRepository', $builder->className());
        $this->tester->assertEquals('app/Foo/Repositories/SyllabiRepository.php', $builder->filePath());
        $this->tester->assertEquals('Foo\Repositories', $builder->getNamespace());
    }

    public function testModelScriptBuilderWorks()
    {
        $builder = new ModelScriptBuilder('Car', 'Acme\Foo', 'app');

        $this->tester->assertEquals('Car', $builder->className());
        $this->tester->assertEquals('app/Acme/Foo/Models/Car.php', $builder->filePath());
        $this->tester->assertEquals('Acme\Foo\Models', $builder->getNamespace());
    }

    public function testInputValidatorScriptBuilderWorks()
    {
        $builder = new InputValidatorScriptBuilder('Request', 'Acme\Requests', 'app');

        $this->tester->assertEquals('RequestInputValidator', $builder->className());
        $this->tester->assertEquals('app/Acme/Requests/Validation/RequestInputValidator.php', $builder->filePath());
        $this->tester->assertEquals('Acme\Requests\Validation', $builder->getNamespace());
    }

}
