<?php 
$I = new FunctionalTester($scenario);

$I->am('ninja developer');
$I->wantTo('generate all elegan files using the elegan:make command');

$I->cleanDir('tests/_output');

$I->runShellCommand('php ../../../artisan elegan:make Task MyApp --path="tests/_output"');
$I->seeInShellOutput('Elegan files was created!');

$I->openFile('tests/_output/MyApp/Controllers/TasksController.php');
$I->seeFileContentsEqual(file_get_contents('tests/_data/TasksController.php'));

$I->openFile('tests/_output/MyApp/Models/Task.php');
$I->seeFileContentsEqual(file_get_contents('tests/_data/Task.php'));

$I->openFile('tests/_output/MyApp/Validation/TaskInputValidator.php');
$I->seeFileContentsEqual(file_get_contents('tests/_data/TaskInputValidator.php'));

$I->openFile('tests/_output/MyApp/Repositories/TasksRepository.php');
$I->seeFileContentsEqual(file_get_contents('tests/_data/TasksRepository.php'));

