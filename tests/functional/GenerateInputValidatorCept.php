<?php 
$I = new FunctionalTester($scenario);

$I->am('ninja developer');
$I->wantTo('generate elegan input validator using the elegan:make command');

$I->cleanDir('tests/_output');

$I->runShellCommand('php ../../../artisan elegan:make --validator Task MyApp --path="tests/_output"');
$I->seeInShellOutput('Elegan files was created!');

$I->openFile('tests/_output/MyApp/Validation/TaskInputValidator.php');
$I->seeFileContentsEqual(file_get_contents('tests/_data/TaskInputValidator.php'));
