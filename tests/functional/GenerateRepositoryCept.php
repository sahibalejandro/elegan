<?php 
$I = new FunctionalTester($scenario);

$I->am('ninja developer');
$I->wantTo('generate elegan repository using the elegan:make command');

$I->cleanDir('tests/_output');

$I->runShellCommand('php ../../../artisan elegan:make --repository Task MyApp --path="tests/_output"');
$I->seeInShellOutput('Elegan files was created!');

$I->openFile('tests/_output/MyApp/Repositories/TasksRepository.php');
$I->seeFileContentsEqual(file_get_contents('tests/_data/TasksRepository.php'));

