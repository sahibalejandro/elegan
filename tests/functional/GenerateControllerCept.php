<?php 
$I = new FunctionalTester($scenario);

$I->am('ninja developer');
$I->wantTo('generate elegan controller using the elegan:make command');

$I->cleanDir('tests/_output');

$I->runShellCommand('php ../../../artisan elegan:make --controller Task MyApp --path="tests/_output"');
$I->seeInShellOutput('Elegan files was created!');

$I->openFile('tests/_output/MyApp/Controllers/TasksController.php');
$I->seeFileContentsEqual(file_get_contents('tests/_data/TasksController.php'));
