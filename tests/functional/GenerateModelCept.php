<?php
$I = new FunctionalTester($scenario);

$I->am('ninja developer');
$I->wantTo('generate elegan model using the elegan:make command');

$I->cleanDir('tests/_output');

$I->runShellCommand('php ../../../artisan elegan:make --model Task MyApp --path="tests/_output"');
$I->seeInShellOutput('Elegan files was created!');

$I->openFile('tests/_output/MyApp/Models/Task.php');
$I->seeFileContentsEqual(file_get_contents('tests/_data/Task.php'));
