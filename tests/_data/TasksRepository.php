<?php namespace MyApp\Repositories;

use Sahib\Elegan\Repositories\Repository;

class TasksRepository extends Repository
{
    /**
     * Model name.
     *
     * @var string
     */
    protected $model = 'MyApp\Models\Task';
}
