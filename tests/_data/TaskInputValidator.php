<?php namespace MyApp\Validation;

use Sahib\Elegan\Validation\InputValidator;

class TaskInputValidator extends InputValidator
{
    /**
     * Validation rules.
     *
     * @var array
     */
    protected $rules = [];

    /**
     * Validation messages.
     *
     * @var array
     */
    protected $messages = [];

    /**
     * Modify validation rules for create mode.
     *
     * @return $this
     */
    public function createMode()
    {
        // TODO: Modify validation rules for create mode.

        return parent::createMode();
    }

    /**
     * Modify validation rules for update mode.
     *
     * @return $this
     */
    public function updateMode(\Eloquent $resource)
    {
        // TODO: Modify validation rules for update mode.

        return parent::updateMode($resource);
    }
}
