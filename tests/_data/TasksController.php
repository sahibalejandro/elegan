<?php namespace MyApp\Controllers;

use Flash;
use Redirect;
use Sahib\Elegan\Controllers\ResourceController;
use MyApp\Repositories\TasksRepository;
use MyApp\Validation\TaskInputValidator;

class TasksController extends ResourceController
{
    /**
     * Name of the variable which represents a single resource on views.
     *
     * @var string
     */
    protected $resource = 'task';

    /**
     * Name of the variable which represents a collection of resources on views.
     *
     * @var string
     */
    protected $resources = 'tasks';

    /**
     * Views prefix.
     *
     * @var string
     */
    protected $viewsPrefix = 'tasks';

    /**
     * Routes prefix.
     *
     * @var string
     */
    protected $routesPrefix = 'tasks';

    /**
     * @param \MyApp\Repositories\TasksRepository $repository;
     * @param \MyApp\Validation\TaskInputValidator $validator;
     */
    public function __construct(TasksRepository $repository, TaskInputValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
    }

    /**
     * @param \Eloquent $resource
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function createdResourceResponse(\Eloquent $resource)
    {
        Flash::message("New task created!");

        return Redirect::route($this->routeName('index'));
    }

    /**
     * @param \Eloquent $resource
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function updatedResourceResponse(\Eloquent $resource)
    {
        Flash::message("The task was updated!");

        return Redirect::route($this->routeName('index'));
    }

    /**
     * @param \Eloquent $resource
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function destroyedResourceResponse(\Eloquent $resource)
    {
        Flash::message("The task was destroyed!");

        return Redirect::route($this->routeName('index'));
    }
}
